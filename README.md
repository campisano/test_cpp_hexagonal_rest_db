[![Build Status](https://gitlab.com/campisano/test_cpp_hexagonal_rest_db/badges/master/pipeline.svg "Build Status")](https://gitlab.com/campisano/test_cpp_hexagonal_rest_db/-/pipelines)
[![Sonar Coverage](https://sonarcloud.io/api/project_badges/measure?project=test_cpp_hexagonal_rest_db&metric=coverage "Sonar Coverage")](https://sonarcloud.io/dashboard?id=test_cpp_hexagonal_rest_db)
[![Sonar Maintainability](https://sonarcloud.io/api/project_badges/measure?project=test_cpp_hexagonal_rest_db&metric=sqale_rating "Sonar Maintainability")](https://sonarcloud.io/dashboard?id=test_cpp_hexagonal_rest_db)
[![Docker Image](https://img.shields.io/docker/image-size/riccardocampisano/public/test_cpp_hexagonal_rest_db-latest?label=test_cpp_hexagonal_rest_db-latest&logo=docker "Docker Image")](https://hub.docker.com/r/riccardocampisano/public/tags?name=test_cpp_hexagonal_rest_db)

# Clean Architecture / Hexagonal C++11 example project using rest and persistence

An application developed to show a sample Clean Architecture / Hexagonal solution in c++11 to provide and consume REST API and store data in a RDBMS.

A docker-compose.yml is provided to rapidly have a local database ready to test the project.



## Usage

* Dependencies

```apt-get install gcc g++ make cmake libspdlog-dev libyaml-cpp-dev libssl-dev```

* Build

```
cd lib; make; cd ..
make debug
```

* Run

```docker-compose up -d```

```make run```
