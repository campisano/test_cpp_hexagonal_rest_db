# mandatory vars:

binary_name			:= example
type				:= exec



# optional vars:
include_paths			:= src/application/inc src/adapters/inc \
                                   lib/doctest/install/include
library_paths			:= src/domain/build/lib src/application/build/lib src/adapters/build/lib

#source_extension		?= .cpp
#source_paths			?= src
libs_to_link			:= spdlog pq pqxx \
                                   :adapters.so :application.so :domain.so

main_source_paths		:= src/main

test_source_paths		:= src/test
#test_add_libs_to_link		?=

#output_folder			?= build
#DESTDIR				?=
PREFIX				:= $(CURDIR)/install

format_dirs			:= src
#format_extensions		?= .hpp .cpp

subcomponent_paths		:= src/domain src/application src/adapters

include lib/generic_makefile/modular_makefile.mk
