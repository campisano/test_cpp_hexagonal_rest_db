#ifndef ENV_CONFIG_PROVIDER__HPP__
#define ENV_CONFIG_PROVIDER__HPP__

#include <string>
#include <adapters/drivers/config_provider.hpp>

class EnvConfigProvider : public ConfigProvider
{
public:
    explicit EnvConfigProvider();
    EnvConfigProvider(const EnvConfigProvider &) = delete;
    EnvConfigProvider(EnvConfigProvider &&) = default;
    virtual ~EnvConfigProvider();

    EnvConfigProvider & operator=(const EnvConfigProvider &) = delete;
    EnvConfigProvider & operator=(EnvConfigProvider &&) = default;

public:
    virtual Config config();
};

#endif
