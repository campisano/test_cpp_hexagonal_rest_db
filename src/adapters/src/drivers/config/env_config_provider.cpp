#include "env_config_provider.hpp"

#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <sstream>
#include <stdexcept>
#include <string>

namespace
{
std::string getEnvStr(std::string _name);
std::int32_t getEnvInt(std::string _name);
}

EnvConfigProvider::EnvConfigProvider()
{
}

EnvConfigProvider::~EnvConfigProvider()
{
}

Config EnvConfigProvider::config()
{
    Config::Http http_cfg
    {
        getEnvStr("HTTP_HOST"),
        getEnvInt("HTTP_PORT"),
        getEnvInt("HTTP_THREADS")
    };

    Config::Logger log_cfg
    {
        getEnvStr("LOGGER_FORMAT"),
        getEnvStr("LOGGER_LEVEL")
    };

    Config::Persistence pers_cfg
    {
        getEnvStr("PERSISTENCE_DRIVER"),
        getEnvStr("PERSISTENCE_PARAMETERS")
    };

    return Config{http_cfg, log_cfg, pers_cfg};
}

namespace
{
std::string getEnvStr(std::string _name)
{
    auto env = std::getenv(_name.c_str());

    if(!env)
    {
        std::stringstream msg;
        msg << "Env var not found: " << _name;
        throw std::runtime_error(msg.str());
    }

    return std::string(env);
}

std::int32_t getEnvInt(std::string _name)
{
    auto env = getEnvStr(_name);


    if(env.end() != std::find_if(env.begin(), env.end(), [](unsigned char c)->bool { return !isdigit(c); }))
    {
        std::stringstream msg;
        msg << "Env var does not contain an integer: " << _name;
        throw std::runtime_error(msg.str());
    }

    return std::stoi(env);
}
}
