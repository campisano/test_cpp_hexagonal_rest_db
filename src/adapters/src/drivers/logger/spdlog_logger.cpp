#include "spdlog_logger.hpp"

#include <spdlog/sinks/daily_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace
{
std::shared_ptr<spdlog::logger> getLogger(std::string _name);
}

SpdlogLogger::SpdlogLogger(const std::string & _format,
                           const std::string & _level)
    : m_name("root")
{
    std::string lcase_level;
    lcase_level.resize(_level.size());
    std::transform(_level.begin(), _level.end(), lcase_level.begin(), ::tolower);

    auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    auto logger = std::make_shared<spdlog::logger>(m_name, console_sink);
    logger->set_level(spdlog::level::from_str(lcase_level));
    logger->set_pattern(_format);
    spdlog::set_default_logger(logger);
}

SpdlogLogger::~SpdlogLogger()
{
}

void SpdlogLogger::debug(
    const std::string & _name, const std::string & _message)
{
    getLogger(_name)->debug(_message);
}

void SpdlogLogger::info(
    const std::string & _name, const std::string & _message)
{
    getLogger(_name)->info(_message);
}

void SpdlogLogger::warn(
    const std::string & _name, const std::string & _message)
{
    getLogger(_name)->warn(_message);
}

void SpdlogLogger::error(
    const std::string & _name, const std::string & _message)
{
    getLogger(_name)->error(_message);
}

namespace
{
std::shared_ptr<spdlog::logger> getLogger(std::string _name)
{
    auto logger = spdlog::get(_name);

    if(!logger)
    {
        logger = spdlog::get("root")->clone(_name);
        spdlog::register_logger(logger);
    }

    return logger;
}
}
