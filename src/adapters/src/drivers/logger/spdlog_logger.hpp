#ifndef SPDLOG_LOGGER__HPP__
#define SPDLOG_LOGGER__HPP__

#include <adapters/drivers/logger.hpp>
#include <string>

class SpdlogLogger : public Logger
{
public:
    explicit SpdlogLogger(const std::string & _format, const std::string & _level);
    SpdlogLogger(const SpdlogLogger &) = delete;
    SpdlogLogger(SpdlogLogger &&) = default;
    virtual ~SpdlogLogger();

    SpdlogLogger & operator=(const SpdlogLogger &) = delete;
    SpdlogLogger & operator=(SpdlogLogger &&) = default;

public:
    static void debug(const std::string & _name, const std::string & _message);
    static void info(const std::string & _name, const std::string & _message);
    static void warn(const std::string & _name, const std::string & _message);
    static void error(const std::string & _name, const std::string & _message);

private:
    std::string m_name;
};

#endif
