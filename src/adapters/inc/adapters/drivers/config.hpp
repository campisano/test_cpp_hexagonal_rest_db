#ifndef CONFIG__HPP__
#define CONFIG__HPP__

#include <string>

struct Config
{
    struct Http
    {
        std::string host;
        int port;
        int threads;
    } http;

    struct Logger
    {
        std::string format;
        std::string level;
    } logger;

    struct Persistence
    {
        std::string driver;
        std::string parameters;
    } persistence;
};

#endif
