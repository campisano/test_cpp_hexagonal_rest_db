#ifndef LOGGER__HPP__
#define LOGGER__HPP__

class Logger
{
public:
    virtual ~Logger() = default;
};

#endif
