#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

STACK_NAME="$(./ci/custom/get_project_name.sh | sed 's/_/-/g')"



# send stack to deploy service
./ci/utils/get_stack_to_deploy.sh | curl -q -sSw "\n\nHTTP code: %{http_code}\n" \
-k --ciphers 'DEFAULT:!DH' \
-X POST "${DEPLOY_URL}/${STACK_NAME}" \
-H "Cache-Control: no-cache" \
-H "Content-Type: application/json" \
-H "Authorization: Bearer ${DEPLOY_TOKEN}" \
--data-binary '@-'



# End
